# Classification of interesting and boring images

This directory contains the code to perform classification
of interesting and boring images.

The classification process follows the sequence:

1. Test image set generation
2. Wavelet transform and vectorisation
3. Classification
4. Evaluation

Each of these processes are explained in detail below:

## Test image set generation

An assumption is that any training image set supplied containing
interesting and boring images will be very unbalanced (many boring and
not many interesting). To balanced the data set and also remove any
rotational effects, the set of images are sampled with replacement to
obtain SAMPLE boring and SAMPLE interesting images (where SAMPLE is
the parameter supplied to the option --sample) to obtain 2xSAMPLE
images. Each of these images is then randomly rotated (by choosing an
angle uniformly from 0 to 360 degrees) and then cropped to remove
blank regions left by the rotation. If the distance from the centre to
the edge of the image is r, the cropped image will have the value
r/sqrt(2).

By supplying rotated images, we attempt to allow the classifier to
learn from the images regardless of the rotation.

## Wavelet transform and vectorisation

The classification stage (stage 3) requires that we provide the each
image as a vector, where the vector elements are measurements of an
image feature. The 2D discrete wavelet transform is able to convert
the set of pixels into measurements of the magnitude of each shifted
and scaled wavelet coefficient. Thresholding the wavelet coefficients
aims to remove noise from the image and allows us to represent the
images more compactly. The wavelet type is set using the command line
option --wavelet, where the choice of wavelets can be seen by
providing the --help option. Soft thresholding is performed (all
coefficients are shifted towards zero by the threshold amount, any
with magnitude less than zero become zero). The threshold is set using
the --thresh option.

The matrices are then flattened into vectors.

## Classification

Classification can be performed using either regularised logistic
regression (--LR) or support vector machine (--SVM).  The
classification process computes model coefficients using the training
data, in order to provide predictions on future data. Each model has
its own set of coefficients that must be learnt.

### Regularised logistic regression

Logistic regression is a linear regression model with a logistic link
function. To fit the model, we must compute the $M$ model
coefficients, associated to the $M$ wavelet values within each
training vector, that provide the maximum likelihood fit to the
training sample.

Regularisation is performed using the L2 norm, reducing the risk of
over fitting the training data.

This model has the parameter $C$ that determines the contribution of
the regularisation term. If $C$ is small, the fitted model will have a
close fit to the training data, with highly variant fitted
coefficients. If $C$ is large, the fitted model will have a loose fit
to the training data, but with less variant coefficients. The value of
$C$ is chosen using cross validation.

### Support vector machine

Support vector machines (SVMs) compute the norm of the hyperplane that
provides the largest margin between the two classes of training
images. Since many training sets are not separable, the SVM allows for
error in the fitted model. Therefore the fitted hyperplane trades off
between maximising the margin and minimising the error, where the
trade off is controlled by the parameter $C$. The SVM parameter $C$
plays a similar role to the logistic regression $C$.

A useful feature of the SVM is the ability to use kernel functions.

- A linear kernel function (inner product) will provide a linear
  separating hyperplane (no additional parameters).

- A polynomial kernel will provide a non-linear hyperplane (using two
  parameters, the polynomial degree and offset).

- A radial basis kernel will provide a non-linear hyperplane (using one
  parameter, the standard deviation).

Cross validation is used to compute the most appropriate value for $C$
and kernel parameters.

## Cross validation

It is tempting to compute the model that provides the best fit to the
training data, meaning is it provides very accurate classification
when using the training data. Unfortunately, high accuracy for the
training data does not imply high accuracy for any data that needs
classification in the future. We want a model that is able to
generalise to new data and not over fit the training data.

We can examine the generalisation of the model by constructing a
validation image set, where we train on the training set, then examine
the accuracy of the model on the validation set over all parameters
($C$ and any kernel parameters). Using the validation set allows us to
examine the model accuracy on new data (not in the training set),
therefore we choose the parameters that provide the greatest accuracy
on the validation set to obtain the model with greatest generalisation
potential.

Usually, we will not have enough data to define a specific training
and validation set, therefore we use cross validation, where the
training set is partitioned into $k$ chunks and we iterate through the
chunks as training and validation sets. Our code uses stratified 5
fold cross validation, where the training set is partitioned into five
chunks and each of the two classes are evenly spread across the
classes.


## Evaluation

It is usual to provide the accuracy of a classification as the
proportion of correctly classified test images, where the test images
have not been used in any of the model fitting stages. For our case,
the number of boring images is far larger than the number of
interesting images, so we would obtain high accuracy by simply
predicting the class of each image as boring. For example, if only 5%
of the test images are interesting, classifying all images as boring
gives 95% accuracy. Therefore, using the proportion of correct
classifications is not a method of evaluating the model.

To evaluate the model, we examine the predicted probability of each
image being interesting. Using these probabilities, we can rank the
complete set of test images from most probable to least probable.  If
a model is able to rank all truly interesting images above all truly
boring images, it has achieved an accuracy of 1. If any truly boring
images are ranked above any truly interesting images, if obtains an
accuracy of less than 1. To obtain this accuracy score we use Average
Precision.

Average precision is the average of the precision of the ranking,
where we compute the precision at each interesting image rank.  The
precision at $k$ is the number of interesting images in the top $k$
ranked images. If there are five interesting images, each ranked 1, 2,
5, 6 and 10. The precision of each are 1/1, 2/2, 3/5, 4/6 and 5/10,
giving an average precision of 0.753 (mean of 1, 1, 0.6, 0.67 and
0.5). Average precision should be used during cross validation and
evaluation of the final model.

The results are printed using four columns, where the columns are
image rank, image file path, image interesting score, run ID.


# Performing the classification

A description of the options are given when providing `--help`.
```{sh}
python src/wave_logistic.py --help
```

An example run is given below:

```{sh}
python src/wave_logistic.py --train data/train_list.txt --test data/test_list.txt \
       --SVM -w "sym8" --thresh 20 --sample 100 --runid "lapark@ws"
```

The above run:

- provides the training and test image path files
  train_list.txt and test_list.txt,
- specifies an SVM model,
- uses the symmlet wavelet with 8 vanishing moments (sym8),
- thresholds the wavelet coefficients at 20,
- obtains a sample of 100 randomly rotated interesting and 100
  randomly rotated boring images from the training set,
- prints the run ID "lapark@ws" in the results list.


To save the results, redirect the output to a file.

```{sh}
python src/wave_logistic.py --train data/train_list.txt --test data/test_list.txt \
       --SVM -w "sym8" --thresh 20 --sample 100 --runid "lapark@ws" > results.txt
```

# Image description files

To perform the classification, two image description files are needed,
containing the path of training images and testing images. The images
are supplied using the options --train and --test. For example:

```{python}
python src/wave_logistic.py --train data/train_list.txt --test data/test_list.txt
```

Each file has one line per image. Each line in the file contains the
class (0 for boring, 1 for interesting) and then the file path,
separated by a space. E.g.

```{txt}
0 ./class/S006_B.png
0 ./class/S008_B.png
0 ./class/S010_B.png
1 ./class/S011_I.png
0 ./class/S013_B.png
```

The above five lines show the paths of five files, where one is
interesting and the others are not.

# Results

The results of the classification are presented as a header of
parameters and results and a ranking of all test images presented in
four columns. An example output is given below.

```{txt}
-- Reading file train_list.txt | wavelet = sym8 | threshold = 10 | sample size = 1000
-- Using logistic regression.
-- Found parameters:  C = 7.62939453125e-06 |
-- Reading file test_list.txt | wavelet = sym8 | threshold = 10
-- Test scores: AP = 0.755359242307 | AP2 = 0.749913676952 | AUC = 0.943620178042
1       ./class/S022_I.png      -3.1936313793e-05       lapark@ws
2       ./class/S336_I.png      -0.0010303019096        lapark@ws
3       ./class/S011_I.png      -0.00317377382626       lapark@ws
4       ./class/S178_I.png      -0.00341601809792       lapark@ws
5       ./class/S352_I.png      -0.0064759244951        lapark@ws
```

The results (AP, AP2 and AUC) are presented in the header. The ranking
and score of each image are presented in the four columns.
