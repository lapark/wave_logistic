#!/usr/bin/python
#
# Convert an image to a set of 2D wavelets then model using logistic regression.
# Copyright (C) 2016 Laurence A. F. Park <lapark@scem.westernsydney.edu.au>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


import scipy
import numpy as np

import pywt
#import pywt.thresholding

from sklearn.linear_model import LogisticRegression
#from sklearn.linear_model import LogisticRegressionCV
from sklearn import svm
#from sklearn import tree

from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.grid_search import GridSearchCV
#from sklearn.model_selection import GridSearchCV
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

import randomRotate
import pdb


def load_images_from_file(image_file, thresh, wavelet):

    print "-- Reading file " + image_file + " | wavelet = " + str(wavelet) + " | threshold = " + str(thresh)

    fileList, imagePool = randomRotate.imagePoolFromFileList(image_file)
    mimage = [imageToVector(image[1], thresh = thresh, wavelet = wavelet)
              for image in imagePool]
    imageClass = [image[0] for image in imagePool]

    return(fileList, mimage, imageClass)


def load_rotated_images_from_file(image_file, thresh, wavelet, sampleSize = 1000):

    print "-- Reading file " + image_file + " | wavelet = " + str(wavelet) + " | threshold = " + str(thresh) + " | sample size = " + str(sampleSize)

    #sampleSize = 1000

    imagePool = randomRotate.rotatedImagePoolFromFileList(image_file, sampleSize)
    mimage = [imageToVector(image[1], thresh = thresh, wavelet = wavelet)
              for image in imagePool]
    imageClass = [image[0] for image in imagePool]

    return(mimage, imageClass)



def vectoriseFile(fileLine, thresh, wavelet):
    line = fileLine.rstrip()
    imageData = line.split(' ') # replace with your own separator instead
    #print imageData
    image = scipy.misc.imread(imageData[1])
    ## perform thresholded 2D wavelet transform and vectorise image

    wimage = imageToVector(image, thresh = thresh, wavelet = wavelet)

    return(imageData[0], wimage)


def vectoriseMatrixSet(matrixSet):
    return(np.hstack(np.concatenate(matrixSet)))


def thresholdArray(x, thresh = 1):
    ## reduce all x towards zero by amount 'thresh'
    ## assume additive Gaussian noise
    magnitude = np.absolute(x)
    sign = np.sign(x)
    thresholded = (magnitude - thresh).clip(0) * sign
    return(thresholded)

def imageToVector(image, thresh = 0, wavelet = "haar"):
    if (wavelet == "None"):
        vectorThresh = thresholdArray(image.flatten(), thresh)
        return(np.int32(vectorThresh))
    else:
        return(imageToWaveletVector(image, thresh, wavelet))

        
def imageToWaveletVector(image, thresh = 0, wavelet = "haar"):

    # apply DWT
    # level = 6 for 64x64 pixel images
    level = np.int32(np.floor(np.log2(image.shape[0])))
    #level = 1

    ##pdb.set_trace()
    ##imageDWT = pywt.wavedec2(image, wavelet, level=level, mode = pywt.MODES.per)
    ## uses pywt.dwt_max_level() to compute depth
    ## https://pywavelets.readthedocs.io/en/0.2.0/ref/dwt-discrete-wavelet-transform.html
    imageDWT = pywt.wavedec2(image, wavelet, level=None, mode = pywt.MODES.per)

    # convert matrices to vectors
    flatDWT = map(vectoriseMatrixSet, imageDWT)
    # join all DWT levels
    vectorDWT = np.concatenate(flatDWT)

    #np.histogram(vectorDWT)
    
    # remove small coefficients
    vectorDWTthresh = thresholdArray(vectorDWT, thresh)
    #vectorDWTthresh = pywt.thresholding.soft(vectorDWT, thresh)

    # convert to integer
    #return(np.int32(vectorDWTthresh))
    return(vectorDWTthresh)


def averagePrecision(imageScores, relevance):
    ## Compute the average precision for a given set of score and relevance judgements.
    ## relevance: a list of 0 and 1's where 1 in position i means image i is relevant.

    #rankedRelevance = np.array([x for (y,x) in sorted(zip(imageScores,relevance))][::-1])
    rankedRelevance = np.array([relevance[i] for i in np.argsort(imageScores)[::-1]])
    #print rankedRelevance
    rel = rankedRelevance.astype(int)
    crel = rel.cumsum()
    pos = [x+1 for x in range(len(crel))]
    prec = [(x*1.0)/y for x, y in zip(crel, pos)]
    AP = sum(rel * prec)/sum(rel)

    return(AP)


def plotCVaccuracy(param_range, trainX, trainRel, modelType, fold):
    ## Plot AP versus C for both training and validation data
    
    import matplotlib.pyplot as plt
    from sklearn.learning_curve import validation_curve

    #param_range = np.power(2.0, np.arange(-30, 10)) #np.logspace(-7, 3, 3)

    train_scores, test_scores = validation_curve(modelType, trainX, trainRel, "C",
                                                 param_range, scoring = "average_precision",
                                                 cv = fold)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    
    plt.title("Validation Curve")
    plt.xlabel("C")
    plt.ylabel("AP")
    plt.ylim(0.0, 1.1)
    plt.semilogx(param_range, train_scores_mean, label="Training score", color="r")
    plt.fill_between(param_range, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.2, color="r")
    plt.semilogx(param_range, test_scores_mean, label="Cross-validation score",
                 color="g")
    plt.fill_between(param_range, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.2, color="g")
    plt.legend(loc="best")
    plt.show()




def modelLogisticRegression2():
    ## use logistic regression

    print("-- Using logistic regression.")
    grid = {
        'C': np.power(2.0, np.arange(-10, 10))
    }

    #modelType = LogisticRegression(penalty = "l2", class_weight = "auto") #class_weight = "balanced") #, solver = "liblinear") #class_weight = "balanced", max_iter=10000, tol=10)
    modelType = LogisticRegression(penalty = "l2", class_weight = "balanced") #, solver = "liblinear") #class_weight = "balanced", max_iter=10000, tol=10)

    return([modelType, grid])

def modelLogisticRegression1():
    ## use logistic regression

    print("-- Using logistic regression.")
    grid = {
        'C': np.power(2.0, np.arange(-10, 10))
    }

    #modelType = LogisticRegression(penalty = "l1", class_weight = "auto") #class_weight = "balanced") #, solver = "liblinear") #class_weight = "balanced", max_iter=10000, tol=10)
    modelType = LogisticRegression(penalty = "l1", class_weight = "balanced") #, solver = "liblinear") #class_weight = "balanced", max_iter=10000, tol=10)

    return([modelType, grid])


def modelSupportVectorMachine():
    ## use a SVM
    ## Note that we could use a one class SVM for outlier detection,
    ## but we are unable to fit the paramters using cross validation.
    
    
    print("-- Using support vector machine.")
    grid = {
        'C': np.power(2.0, np.arange(-10, 10)),
        #'gamma': np.power(2.0, np.arange(-10, 10))
        #'gamma': 'auto'
        'degree': [1,2,3],
        'coef0': [1,2,3]
    }

    #modelType = svm.SVC(kernel="rbf", probability=True, class_weight="balanced") #gamma="auto",
    modelType = svm.SVC(kernel="poly", probability=True, class_weight="balanced")
    #modelType = svm.SVC(kernel="poly", probability=True, class_weight="auto") #class_weight="balanced")
    #modelType = svm.SVC(kernel="linear", probability=True, class_weight="balanced")
    
    return([modelType, grid])


    
def classifyImages(trainFile, testFile, modelParams):

    ## choose wavelet from pywt.wavelist() or None
    wavelet = modelParams['wavelet']
    ## all wavelet coefficients are soft thresholded to remove noise
    threshold = modelParams['threshold']

    sampleSize = modelParams['sampleSize']
    ## train model
    if (modelParams['rotate'] == "rotate"):
        trainX, trainy = load_rotated_images_from_file(trainFile, thresh = threshold,
                                                       wavelet = wavelet, sampleSize = sampleSize)

    if (modelParams['rotate'] == "norotate"):
        fileList, trainX, trainy = load_images_from_file(trainFile, thresh = threshold, wavelet = wavelet)

    ## convert labels to numeric
    trainRel = [int(x) for x in trainy]

    
    ## fit model using cross validation
    
    ## use stratified cross validation to ensure that positive cases are
    ## in each fold
    #fold = StratifiedShuffleSplit(trainRel, n_iter=10, test_size=0.5) #, random_state=0)
    #Need to validate using validate set, not cross validate.
    fold = KFold(len(trainy), n_folds=5, shuffle=True) #, random_state=777)
    #fold = StratifiedKFold(trainy, n_folds=5, shuffle=True) #, random_state=777)
    

    ## choose a model
    if (modelParams['model'] == "LR1"):
        modelType, grid = modelLogisticRegression1()
    if (modelParams['model'] == "LR2"):
        modelType, grid = modelLogisticRegression2()
    if (modelParams['model'] == "SVM"):
        modelType, grid = modelSupportVectorMachine()

    
    model = GridSearchCV(modelType, grid, scoring='average_precision', cv=fold,
                         refit = True, n_jobs = -1)
    #model = GridSearchCV(modelType, grid, cv=fold, refit = True)

    
    model.fit(trainX, trainRel)
    print "-- Found parameters: ",
    for k,v in model.best_params_.items():
        print k + " = " + str(v) + " |",
    print "\n",

    ## Examine the CV accuracy
    #plotCVaccuracy(grid['C'], trainX, trainRel, modelType, fold)
    
    ## test model
    fileList, testX, testy = load_images_from_file(testFile, thresh = threshold,
                                                   wavelet = wavelet)
    testRel = [int(x) for x in testy]
    #p = model.predict_log_proba(testX)
    p = model.predict_proba(testX)
    interestingProb = p[:,1]
    #pdb.set_trace()

    
    ## evaluate classifiation using AP and AuC
    ap = averagePrecision(interestingProb, testRel)
    #ap2 = average_precision_score(testRel, interestingProb, average = None)  
    ap2 = average_precision_score(testRel, interestingProb)  
    auc = roc_auc_score(testRel, interestingProb)  
    print "-- Test scores: AP = " + str(ap) + " | AP2 = " + str(ap2) + " | AUC = " + str(auc)


    ## save results in file
    rank = 1
    for (p,f) in sorted(zip(interestingProb,fileList))[::-1]:
        print str(rank) + "\t" + f + "\t" + str(p) + "\t" + modelParams['runID']
        rank += 1
    
    ## examine coefficients
    #print model.best_estimator_.coef_[0]


#def printPrediction(train

    
import argparse

def main():

    parser = argparse.ArgumentParser(description='Train and classify interesting images.')
    parser.add_argument('-t','--train', required=True,
                        help='Provide the list of training images.')
    parser.add_argument('-s','--test', required=True,
                        help='Provide the list of test images.')

    groupMethod = parser.add_mutually_exclusive_group(required=True)
    groupMethod.add_argument('--LR1', action='store_const',
                             const="LR1", dest="method",
                             help='Use the logistic regression model with 1 norm regularisation.')
    groupMethod.add_argument('--LR2', action='store_const',
                             const="LR2", dest="method",
                             help='Use the logistic regression model with 2 norm regularisation.')
    groupMethod.add_argument('--SVM',action='store_const',
                             const="SVM", dest="method",
                             help='Use the support vector machine model.')

    rotateMethod = parser.add_mutually_exclusive_group(required=True)
    rotateMethod.add_argument('--rotate', action='store_const',
                             const="rotate", dest="rotate",
                              help='Use randomly rotated images.')
    rotateMethod.add_argument('--norotate',action='store_const',
                              const="norotate", dest="rotate",
                              help='Don\'t use randomly rotated images.')

    parser.add_argument('-w','--wavelet', required=False, default="sym8",
                        help='Wavelet to be used with DWT (use None for none or ' + ", ".join(pywt.wavelist()))
    parser.add_argument('-e','--thresh', required=False, default=10,
                        help='Soft thresholding applied after DWT.')
    parser.add_argument('-m','--sample', required=False, default=1000,
                        help='Sample size of each class of randomly rotated training images.')

    parser.add_argument('-i','--runid', required=False, default="wave_logistic",
                        help='The run ID printed in the results output.')

    args = parser.parse_args()

    modelParams = []
    modelParams = {
        'model': args.method,
        'wavelet': args.wavelet,
        'threshold': int(args.thresh),
        'sampleSize': int(args.sample),
        'rotate': args.rotate,
        'runID': args.runid
    }
    
    classifyImages(args.train,args.test, modelParams)
        
if __name__ == '__main__':
    main()

