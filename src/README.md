# Convolutional Neural Network

This code was adapted from the code available at
<http://deeplearning.net/tutorial/lenet.html>.

This neural network contains three layers:

- two convolutional layers (layers 1 and 2)
- one logistic regression layer (layer 3)

The convolutional layers learns the coefficients for a set of filters,
one per neuron. The logistic regression layer learns a vector of
weights for each class from the data.

The model used stochastic gradient descent to optimise the filter and
vector weights.

To run the code, make sure the paths of the image files are correctly stated in:

- test_list.txt
- train_list.txt

then run:

```{sh}
python cnn/convolutional_mlp.py
```

There are many parameters that can be set. See the header of
`cnn/convolutional_mlp.py` for a description of these parameters.

Note that the validation of the model is performed using average
precision. This allows us to fit the model very unballanced classes.

The model performs training and validation. At the moment, the
training images are a random sample with replacement from the training
set, where each image is randomly rotated. This allows us to obtain a
balanced training set, and also attempts to remove rotational
dependence of the images.

The validation is performed on whole test set, unrotated.

The testing is performed on the test set, unrotated.
