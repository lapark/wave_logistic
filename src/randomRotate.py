#!/usr/bin/python
#
# Functions to read images from a file list, randomly rotate and crop them.
# Copyright (C) 2016 Lauernce A. F. Park <lapark@westernsydney.edu.au>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Useful functions:
- imagePoolFromFileList(imageFile) 
  Read all images from the list "imageFile" and crop.

- rotatedImagePoolFromFileList(imageFile, sampleSize):
  Read a random sample of images with replacement from the list "imageFile", randomly 
  rotate them and crop to remove blank space.

"""


import scipy
import scipy.ndimage

import numpy as np
import skimage.transform as skt
from skimage import img_as_ubyte

import scipy.ndimage.interpolation as sciint

#import pdb

def cleanLine(line):
    line = line.rstrip()
    line = line.split(" ")
    return(line)

def chooseClass(record, imageClass):
    if (record[0] == imageClass):
        return(True)
    else:
        return(False)


def cropImage(image, ratio = 1.0/np.sqrt(2)):
    ## crop to remove black edges
    d1 = image.shape[0]
    d2 = np.int32(np.floor(d1/2*ratio))*2
    lower = (d1 - d2)/2
    upper = lower + d2
    #pdb.set_trace()
    return(image[lower:upper, lower:upper])

    
def readAndRotate(fileList, angle, shift):
    image = scipy.misc.imread(fileList[1])    
    image = sciint.shift(image, shift, output=None, order=3, mode='wrap', prefilter=True)
    if (angle != 0):
        #pdb.set_trace()
        #image1 = scipy.misc.imrotate(image, angle, interp='bilinear')
        image = img_as_ubyte(skt.rotate(image, angle, resize = False, center = None))

    #pdb.set_trace()

    ## crop to remove black edges
    image = cropImage(image)
    #d1 = image.shape[0]
    #d2 = np.int32(np.floor(d1/2/np.sqrt(2)))*2
    #lower = (d1 - d2)/2
    #upper = lower + d2
    #pdb.set_trace()

    return([fileList[0], image])

def createRotatedImagePool(imagePool, sampleSize):
    N = len(imagePool)

    ## randomly select from the pool of images
    sampleIndex = np.random.choice(N, sampleSize, replace = True)
    imageSamplePool = [imagePool[i] for i in sampleIndex]

    ## uniform randomly choosed rotation angles
    randomAngles = np.random.uniform(0, 360, sampleSize)
    randomShifts = np.random.normal(loc = 0, scale = 5, size = (sampleSize,2))
    rotatedImagePool = [readAndRotate(imageSamplePool[i], randomAngles[i], randomShifts[i,]) for i in range(sampleSize)]

    return(rotatedImagePool)


def rotatedImagePoolFromFileList(imageFile, sampleSize):
    with open(imageFile, 'r') as f:
        imagePool = [cleanLine(line) for line in f]

    negativePool = filter(lambda(x): chooseClass(x, "0"), imagePool)
    positivePool = filter(lambda(x): chooseClass(x, "1"), imagePool)

    rotatedNegativePool = createRotatedImagePool(negativePool, sampleSize)
    rotatedPositivePool = createRotatedImagePool(positivePool, sampleSize)

    rotatedPool = rotatedNegativePool + rotatedPositivePool
    shuffleIndex = np.random.choice(len(rotatedPool), len(rotatedPool), replace = False)

    ## shuffle 1s and 0s
    return([rotatedPool[i] for i in shuffleIndex])



def imagePoolFromFileList(imageFile):
    with open(imageFile, 'r') as f:
        imagePool = [cleanLine(line) for line in f]

    fileList = [imageLine[1] for imageLine in imagePool]
    croppedImagePool = [readAndRotate(imageLine, angle = 0, shift = [0,0]) for imageLine in imagePool]

    ## shuffle 1s and 0s
    return(fileList, croppedImagePool)



def main():
    ## example of use
    
    imageFile = './train_list.txt'
    sampleSize = 3

    imagePool = rotatedImagePoolFromFileList(imageFile, sampleSize)

    import matplotlib.pyplot as plt
    plt.imshow(imagePool[1290][1]); plt.gray(); plt.show()
